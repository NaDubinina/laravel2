<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\ComplaintAndSuggestion\Database\Seeders\ComplaintSeeder;
use Modules\ComplaintAndSuggestion\Database\Seeders\StatusSeeder;
use Modules\ComplaintAndSuggestion\Database\Seeders\SuggestionSeeder;
use Modules\User\Database\Seeders\UserSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            StatusSeeder::class,
            ComplaintSeeder::class,
            SuggestionSeeder::class,
        ]);
    }
}
