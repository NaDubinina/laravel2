<?php

namespace Modules\ComplaintAndSuggestion\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\ComplaintAndSuggestion\Database\factories\ComplaintFactory;
use Modules\User\Entities\User;
use Carbon\Carbon;

/**
 * @property integer        $id
 * @property string         $text
 * @property integer        $status_id
 * @property integer        $user_id
 * @property boolean        $is_visible
 * @property string         $image
 * @property Carbon|null    $created_at
 * @property Carbon|null    $updated_at
 *
 * @property-read User      $user
 * @property-read Status    $status
 *
 * @static ComplaintFactory newFactory
 *
 * @extends Model
 */
class Complaint extends Model
{
    use HasFactory;

    public const FIELD_ID = 'id';
    public const FIELD_TEXT = 'text';
    public const FIELD_STATUS_ID = 'status_id';
    public const FIELD_USER_ID = 'user_id';
    public const FIELD_IS_VISIBLE = 'is_visible';
    public const FIELD_IMAGE = 'image';
    public const TABLE_NAME = 'complaints';
    public const NUMBER_CARD_PER_PAGE = 5;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_ID,
        self::FIELD_TEXT,
        self::FIELD_STATUS_ID,
        self::FIELD_IS_VISIBLE,
        self::FIELD_USER_ID,
        self::FIELD_IMAGE
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class);
    }

    protected static function newFactory()
    {
        return new ComplaintFactory();
    }
}
