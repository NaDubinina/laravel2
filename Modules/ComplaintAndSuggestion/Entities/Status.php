<?php

namespace Modules\ComplaintAndSuggestion\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Carbon\Carbon;

/**
 * @property integer           $id
 * @property string            $name
 * @property Carbon|null       $created_at
 * @property Carbon|null       $updated_at
 *
 * @property-read Complaint[]  $complaints
 * @property-read Suggestion[] $suggestions
 *
 * @extends Model
 */
class Status extends Model
{
    use HasFactory;

    public const FIELD_ID = 'id';
    public const FIELD_NAME = 'name';
    public const TABLE_NAME = 'statuses';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_ID,
        self::FIELD_NAME
    ];

    public function complaints(): HasMany
    {
        return $this->hasMany(Complaint::class);
    }

    public function suggestions(): HasMany
    {
        return $this->hasMany(Suggestion::class);
    }
}
