<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\ComplaintAndSuggestion\Entities\Status;
use Modules\ComplaintAndSuggestion\Entities\Suggestion;
use Modules\User\Entities\User;

class CreateSuggestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Suggestion::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->text(Suggestion::FIELD_TEXT);
            $table->foreignIdFor(Status::class)
                ->default(1)
                ->constrained();
            $table->foreignIdFor(User::class)
                ->constrained()
                ->cascadeOnDelete();
            $table->boolean(Suggestion::FIELD_IS_VISIBLE)->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Suggestion::TABLE_NAME);
    }
}
