<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\ComplaintAndSuggestion\Entities\Complaint;
use Modules\ComplaintAndSuggestion\Entities\Status;
use Modules\User\Entities\User;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Complaint::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->text(Complaint::FIELD_TEXT);
            $table->foreignIdFor(Status::class)
                ->default(1)
                ->constrained();
            $table->foreignIdFor(User::class)
                ->constrained()
                ->cascadeOnDelete();;
            $table->boolean(Complaint::FIELD_IS_VISIBLE)->default(false);
            $table->text(Complaint::FIELD_IMAGE)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Complaint::TABLE_NAME);
    }
}
