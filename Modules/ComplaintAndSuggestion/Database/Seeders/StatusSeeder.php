<?php

namespace Modules\ComplaintAndSuggestion\Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use Modules\ComplaintAndSuggestion\Entities\Status;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(Status::TABLE_NAME)->insert([
            Status::FIELD_NAME => 'Необработано',
        ]);

        DB::table(Status::TABLE_NAME)->insert([
            Status::FIELD_NAME => 'Принято',
        ]);

        DB::table(Status::TABLE_NAME)->insert([
            Status::FIELD_NAME => 'Бред',
        ]);

        DB::table(Status::TABLE_NAME)->insert([
            Status::FIELD_NAME => 'Исправлено',
        ]);
    }
}
