<?php

namespace Modules\ComplaintAndSuggestion\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\ComplaintAndSuggestion\Entities\Complaint;
class ComplaintSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        Complaint::factory()->count(10)->create();
    }
}
