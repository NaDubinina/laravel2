<?php

namespace Modules\ComplaintAndSuggestion\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\ComplaintAndSuggestion\Entities\Suggestion;

class SuggestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Suggestion::factory()->count(10)->create();
    }
}
