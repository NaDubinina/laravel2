<?php

namespace Modules\ComplaintAndSuggestion\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Modules\ComplaintAndSuggestion\Entities\Complaint;
use Modules\User\Entities\User;

class ComplaintFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Complaint::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            Complaint::FIELD_TEXT => $this->faker->text(100),
            Complaint::FIELD_USER_ID => User::where(User::FIELD_IS_ADMIN, 0)->inRandomOrder()->first()->id,
            Complaint::FIELD_IMAGE => Storage::url('complaints' . DIRECTORY_SEPARATOR . $this->faker->
                image(
                    storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'complaints'),
                    80, 80, null, false
                )
            ),
        ];
    }
}
