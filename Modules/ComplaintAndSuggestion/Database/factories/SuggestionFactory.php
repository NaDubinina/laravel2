<?php

namespace Modules\ComplaintAndSuggestion\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\ComplaintAndSuggestion\Entities\Suggestion;
use Modules\User\Entities\User;

class SuggestionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Suggestion::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            Suggestion::FIELD_TEXT => $this->faker->text(100),
            Suggestion::FIELD_USER_ID => User::query()->where(User::FIELD_IS_ADMIN, 0)
                ->inRandomOrder()
                ->first()->id,
        ];
    }
}
