<?php

namespace Modules\ComplaintAndSuggestion\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\ComplaintAndSuggestion\Entities\Complaint;
use Modules\ComplaintAndSuggestion\Entities\Status;
use Modules\ComplaintAndSuggestion\Entities\Suggestion;
use Modules\ComplaintAndSuggestion\Http\Requests\SuggestionRequest;

class SuggestionController extends Controller
{
    public function getVisibleSuggestions()
    {
        $suggestions = Suggestion::query()
            ->where(Suggestion::FIELD_IS_VISIBLE, true)
            ->paginate(Suggestion::NUMBER_CARD_PER_PAGE);

        return view('complaintandsuggestion::suggestions.index', compact('suggestions'));
    }

    public function getUserSuggestions()
    {
        $suggestions = (Auth::user())
            ->suggestions()
            ->paginate(Suggestion::NUMBER_CARD_PER_PAGE);

        return view('complaintandsuggestion::suggestions.index', compact('suggestions'));
    }

    public function getAllSuggestions()
    {
        $suggestions = Suggestion::query()->paginate(Suggestion::NUMBER_CARD_PER_PAGE);

        return view('complaintandsuggestion::suggestions.index', compact('suggestions'));
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('complaintandsuggestion::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('complaintandsuggestion::suggestions.form');
    }

    /**
     * Store a newly created resource in storage.
     * @param SuggestionRequest $request
     * @return Renderable
     */
    public function store(SuggestionRequest $request)
    {
        $params = $request->all();
        $params[Suggestion::FIELD_USER_ID] = Auth::id();
        Suggestion::create($params);

        return redirect()->route('home');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('complaintandsuggestion::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $suggestion = Suggestion::query()->findOrFail($id);
        $statuses = Status::all();
        return view(
            'complaintandsuggestion::suggestions.form',
            compact('suggestion', 'statuses')
        );
    }

    /**
     * Update the specified resource in storage.
     * @param SuggestionRequest $request
     * @param Suggestion $suggestion
     * @return Renderable
     */
    public function update(SuggestionRequest $request, Suggestion $suggestion)
    {
        $params = $request->all();
        $params[Suggestion::FIELD_IS_VISIBLE] = $request->has(Suggestion::FIELD_IS_VISIBLE);
        $suggestion->update($params);

        return redirect()->route('showAllSuggestions');
    }

    /**
     * Remove the specified resource from storage.
     * @param Suggestion $suggestion
     * @return Renderable
     */
    public function destroy(Suggestion $suggestion)
    {
        $suggestion->delete();
        return \Redirect::route('showAllSuggestions');
    }
}
