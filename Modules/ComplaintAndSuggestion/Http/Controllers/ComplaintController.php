<?php

namespace Modules\ComplaintAndSuggestion\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\ComplaintAndSuggestion\Entities\Complaint;
use Modules\ComplaintAndSuggestion\Entities\Status;
use Modules\ComplaintAndSuggestion\Http\Requests\ComplaintRequest;
use Storage;

class ComplaintController extends Controller
{
    public function getVisibleComplaints()
    {
        $complaints = Complaint::query()
            ->where(Complaint::FIELD_IS_VISIBLE, true)
            ->paginate(Complaint::NUMBER_CARD_PER_PAGE);

        return view('complaintandsuggestion::complaints.index', compact('complaints'));
    }

    public function getUserComplaints()
    {
        $complaints = (Auth::user())
            ->complaints()
            ->paginate(Complaint::NUMBER_CARD_PER_PAGE);

        return view('complaintandsuggestion::complaints.index', compact('complaints'));
    }

    public function getAllComplaints()
    {
        $complaints = Complaint::query()->paginate(Complaint::NUMBER_CARD_PER_PAGE);

        return view('complaintandsuggestion::complaints.index', compact('complaints'));
    }


    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('complaintandsuggestion::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('complaintandsuggestion::complaints.form');
    }

    /**
     * Store a newly created resource in storage.
     * @param ComplaintRequest $request
     * @return RedirectResponse
     */
    public function store(ComplaintRequest $request)
    {
        $params = $request->all();
        $params = $this->saveImage($params, $request);
        $params[Complaint::FIELD_USER_ID] = Auth::id();
        Complaint::create($params);

        return redirect()->route('home');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('complaintandsuggestion::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $complaint = Complaint::query()->findOrFail($id);
        $statuses = Status::all();
        return view(
            'complaintandsuggestion::complaints.form',
            compact('complaint', 'statuses')
        );
    }

    /**
     * Update the specified resource in storage.
     * @param ComplaintRequest $request
     * @param Complaint $complaint
     * @return RedirectResponse
     */
    public function update(ComplaintRequest $request, Complaint $complaint)
    {
        $params = $request->all();
        $params[Complaint::FIELD_IS_VISIBLE] = $request->has(Complaint::FIELD_IS_VISIBLE);
        $params = $this->saveImage($params, $request, $complaint);
        $complaint->update($params);

        return redirect()->route('showAllComplaints');
    }

    /**
     * Remove the specified resource from storage.
     * @param Complaint $complaint
     * @return RedirectResponse
     */
    public function destroy(Complaint $complaint)
    {
        $complaint->delete();
        return \Redirect::route('showAllComplaints');
    }

    protected function saveImage($params, $request, $complaint = null)
    {
        unset($params[Complaint::FIELD_IMAGE]);
        if ($request->has(Complaint::FIELD_IMAGE)) {
            if (isset($complaint)) {
                Storage::delete($complaint->image);
            }

            $params[Complaint::FIELD_IMAGE] = $request->file(Complaint::FIELD_IMAGE)->store('complaints');
            $params[Complaint::FIELD_IMAGE] = Storage::url(DIRECTORY_SEPARATOR . $params[Complaint::FIELD_IMAGE]);
        }

        return $params;
    }
}
