<?php

namespace Modules\ComplaintAndSuggestion\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\ComplaintAndSuggestion\Entities\Complaint;

class ComplaintRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            Complaint::FIELD_TEXT => 'required|min:10|max:255',
            Complaint::FIELD_IMAGE => 'image|max:8196',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => __(
                'complaintandsuggestion::validation.required',
                ['attribute' => __('complaintandsuggestion::validation.attributes.text')]
            ),
            'min' => __(
                'complaintandsuggestion::validation.min',
                ['attribute' => __('complaintandsuggestion::validation.attributes.text'), 'min' => ':min']
            ),
            'max' => __(
                'complaintandsuggestion::validation.max.text',
                ['attribute' => __('complaintandsuggestion::validation.attributes.text'), 'max' => ':max'],
            ),
            'max.image' => __(
                'complaintandsuggestion::validation.max.image',
                ['attribute' => __('complaintandsuggestion::validation.attributes.image'), 'max' => ':max']
            ),
            'image' => __(
                'complaintandsuggestion::validation.image',
                ['attribute' => __('complaintandsuggestion::validation.attributes.image')]),
        ];
    }
}
