<?php

return [
    'required' => 'The :attribute field is required to be entered',
    'min' => 'The :attribute field must have a minimum of :min characters',
    'max' => [
        'text' => 'The :attribute field must have a maximum of :max characters',
        'image' => 'The :attribute must not be greater than :max kilobytes'
    ],
    'image' => 'Only an image can be loaded into the :attribute field',
    'attributes' => [
        'text' => 'text',
        'image' => 'image'
    ]
];
