<?php

return [
    'required' => 'Поле :attribute обязательно для ввода',
    'min' => 'Поле :attribute должно иметь минимум :min символов',
    'max' => [
        'text' => 'Поле :attribute должно иметь максимум :max символов',
        'image' => 'Размер :attribute не должен превышать :max килобайт',
    ],
    'image' => 'В поле :attribute возможно загрузить только изображение',
    'attributes' => [
        'text' => 'текст',
        'image' => 'изображение'
    ]
];
