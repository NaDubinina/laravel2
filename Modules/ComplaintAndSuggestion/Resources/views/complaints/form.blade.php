@extends('complaintandsuggestion::layouts.master')

@isset($complaint)
    @section('title', 'Редактировать жалобу #' . $complaint->id)
@else
    @section('title', 'Добавить жалобу')
@endisset

@section('content')
    <div>
        @isset($complaint)
            <h1>Редактировать жалобу #{{ $complaint->id }}</h1>
        @else
            <h1>Добавить жалобу</h1>
        @endisset

        <form method="POST" enctype="multipart/form-data"
              @isset($complaint)
                  action="{{ route('complaints.update', $complaint) }}"
              @else
                  action="{{ route('complaints.store') }}"
            @endisset
        >
            <div>
                @isset($complaint)
                    @method('PUT')
                @endisset
                @csrf
                <div>
                    <label for="code">Текст: </label>
                    <div>
                        @error('text')
                        <div>{{ __($message) }}</div>
                        @enderror
                        <input type="text" name="text" id="text"
                               value="{{ old('text', isset($complaint) ? $complaint->text : null) }}">
                    </div>
                </div>
                @if(Auth::user()->isAdmin() && isset($complaint))
                    <br>
                    <div>
                        <br>
                        <div>
                            <label for="status_id" >Статус: </label>
                            <div>
                                <select name="status_id" id="status_id" class="form-control">
                                    @foreach($statuses as $status)
                                        <option value="{{ $status->id }}"
                                                @isset($complaint)
                                                    @if($complaint->status_id == $status->id)
                                                        selected
                                                @endif
                                                @endisset
                                        >{{ $status->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br>
                    </div>
                    <br>
                    <div>
                        <label for="code">Видимость для пользователей: </label>
                        <div>
                            @error('is_visible')
                            <div>{{ $message }}</div>
                            @enderror
                            <input type="checkbox" name="is_visible" id="is_visible" value="{{ $complaint->is_visible}}"

                                   @if($complaint->is_visible !== 0)
                                       checked="checked"
                                   @endif
                            >
                        </div>
                    </div>
                @endif
                <br>
                <div>
                    <label for="image">Картинка: </label>
                    @isset($complaint->image)
                        <img src="{{ $complaint->image }}">
                    @endisset
                    <div>
                        <label>
                            Загрузить <input type="file" style="display: none;" name="image" id="image">
                        </label>
                    </div>
                </div>
                <button>Сохранить</button>
            </div>
        </form>
    </div>
@endsection
