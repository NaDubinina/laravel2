@extends('complaintandsuggestion::layouts.master')

@section('title', 'Жалобы')
@section('content')
    <button><a class="main-button" href="{{ route('complaints.create') }}">Оставить жалобу</a></button>
    <div>
        <div class="row">
            @foreach($complaints as $complaint)
                @include('complaintandsuggestion::layouts.complaint-card', compact('complaint'))
            @endforeach
        </div>
    </div>

    {{ $complaints->links() }}
@endsection


