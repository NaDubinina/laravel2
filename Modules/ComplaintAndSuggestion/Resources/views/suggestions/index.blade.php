@extends('complaintandsuggestion::layouts.master')

@section('title', 'Предложения')
@section('content')
    <button><a class="main-button" href="{{ route('suggestions.create') }}">Оставить предложение</a></button>
    <div>
        <div class="row">
            @foreach($suggestions as $suggestion)
                @include('complaintandsuggestion::layouts.suggestion-card', compact('suggestion'))
            @endforeach
        </div>
    </div>

    {{ $suggestions->links() }}
@endsection


