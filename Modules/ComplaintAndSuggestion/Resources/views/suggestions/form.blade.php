@extends('complaintandsuggestion::layouts.master')

@isset($suggestion)
    @section('title', 'Редактировать предложение #' . $suggestion->id)
@else
    @section('title', 'Добавить предложение')
@endisset

@section('content')
    <div>
        @isset($suggestion)
            <h1>Редактировать предложение #{{ $suggestion->id }}</h1>
        @else
            <h1>Добавить предложение</h1>
        @endisset

        <form method="POST" enctype="multipart/form-data"
              @isset($suggestion)
                  action="{{ route('suggestions.update', $suggestion) }}"
              @else
                  action="{{ route('suggestions.store') }}"
            @endisset
        >
            <div>
                @isset($suggestion)
                    @method('PUT')
                @endisset
                @csrf
                <div>
                    <label for="code">Текст: </label>
                    <div>
                        @error('text')
                        <div>{{ $message }}</div>
                        @enderror
                        <input type="text" name="text" id="text"
                               value="{{ old('text', isset($suggestion) ? $suggestion->text : null) }}">
                    </div>
                </div>
                <br>
                    @if(Auth::user()->isAdmin() && isset($suggestion))
                        <br>
                        <div>
                            <br>
                            <div>
                                <label for="status_id" >Статус:  </label>
                                <div>
                                    <select name="status_id" id="status_id" class="form-control">
                                        @foreach($statuses as $status)
                                            <option value="{{ $status->id }}"
                                                    @isset($suggestion)
                                                        @if($suggestion->status_id == $status->id)
                                                            selected
                                                    @endif
                                                    @endisset
                                            >{{ $status->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <br>
                        </div>
                        <br>
                        <div>
                            <label for="code">Видимость для пользователей: </label>
                            <div>
                                @error('is_visible')
                                <div>{{ $message }}</div>
                                @enderror
                                <input type="checkbox" name="is_visible" id="is_visible" value="{{ $suggestion->is_visible}}"

                                       @if($suggestion->is_visible !== 0)
                                           checked="checked"
                                        @endif
                                >
                            </div>
                        </div>
                    @endif
                <button>Сохранить</button>
            </div>
        </form>
    </div>
@endsection
