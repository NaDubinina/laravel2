<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ЖП: @yield('title')</title>

    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a class="main-link" href="{{ route('home') }}">Главная</a></li>
                    @guest
                        <li> <a class="main-link" href="{{ route('showComplaintsForGuest') }}">Все жалобы</a></li>
                        <li> <a class="main-link" href="{{ route('showSuggestionsForGuest') }}">Все предложения</a></li>
                    @else
                        @if(Auth::user()->isAdmin())
                            <li> <a class="main-link" href="{{ route('showAllComplaints') }}">Жалобы</a></li>
                            <li> <a class="main-link" href="{{ route('showAllSuggestions') }}">Предложения</a></li>
                        @else
                            <li> <a class="main-link" href="{{ route('showComplaintsForUser') }}">Мои жалобы</a></li>
                            <li> <a class="main-link" href="{{ route('showSuggestionsForUser') }}">Мои предложения</a></li>
                        @endif
                    @endguest
                </ul>

                @guest
                    <ul class="nav navbar-nav navbar-right">
                        <li class="nav-item">
                            <a class="main-link" href="{{ route('login') }}">Войти</a>
                        </li>
                        <li class="nav-item">
                            <a class="main-link" href="{{ route('register') }}">Зарегистрироваться</a>
                        </li>
                    </ul>
                @endguest

                @auth
                    <ul class="nav navbar-nav navbar-right">
                        <li class="nav-item dropdown">
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="main-link" href="{{ route('logout')}}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Выйти
                                </a>

                                <form id="logout-form" action="{{ route('logout')}}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                @endauth
            </div>
        </div>
    </nav>

    <div class="py-4">
        <div class="container">
            <div class="row justify-content-center">
                @yield('content')
            </div>
        </div>
    </div>
</div>
</body>
</html>
