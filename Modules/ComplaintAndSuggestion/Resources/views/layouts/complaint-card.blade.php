<div class="card">
    <div class="main-link"> Имя пользователя: {{$complaint->user->name}}</div>
    <div class="main-link"> Дата создания: {{$complaint->created_at}}</div>
    <div class="main-link"> Жалоба: {{$complaint->text}}</div>
    <div class="main-link"> Статус: {{$complaint->status->name}}</div>
    <img width="300" height="300" src="{{$complaint->image}}">
    @if(isset(Auth::user()->id) && Auth::user()->isAdmin())
        <button><a class="main-link" href="{{ route('complaints.edit', $complaint) }}">Редактировать жалобу</a></button>

        <form action="{{ route('complaints.destroy', $complaint) }}" method="POST">
            @csrf
            @method('DELETE')
            <input type="submit" value="Удалить">
        </form>
    @endif

</div>
