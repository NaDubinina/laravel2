<div class="card">
    <div class="main-link"> Имя пользователя: {{$suggestion->user->name}}</div>
    <div class="main-link"> Дата создания: {{$suggestion->created_at}}</div>
    <div class="main-link"> Жалоба: {{$suggestion->text}}</div>
    <div class="main-link"> Статус: {{$suggestion->status->name}}</div>
    @if(isset(Auth::user()->id) && Auth::user()->isAdmin())
        <button><a class="main-link" href="{{ route('suggestions.edit', $suggestion) }}">Редактировать жалобу</a></button>

        <form action="{{ route('suggestions.destroy', $suggestion) }}" method="POST">
            @csrf
            @method('DELETE')
            <input type="submit" value="Удалить">
        </form>
    @endif
</div>
