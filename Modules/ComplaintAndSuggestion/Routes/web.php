<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\ComplaintAndSuggestion\Http\Controllers\ComplaintController;
use Modules\ComplaintAndSuggestion\Http\Controllers\SuggestionController;

Route::middleware('auth')->group(function () {
    Route::middleware('admin')->group(function () {
        Route::get('/complaints-list', [ComplaintController::class, 'getAllComplaints'])
            ->name('showAllComplaints');
        Route::resource('complaints', 'ComplaintController');

        Route::get('/suggestions-list', [SuggestionController::class, 'getAllSuggestions'])
            ->name('showAllSuggestions');
        Route::resource('suggestions', 'SuggestionController');
    });

    Route::get('/my-complaints', [ComplaintController::class, 'getUserComplaints'])
        ->name('showComplaintsForUser');
    Route::get('/complaints/create', [ComplaintController::class, 'create'])
        ->name('complaints.create');
    Route::post('/complaints', [ComplaintController::class, 'store'])
        ->name('complaints.store');

    Route::get('/my-suggestions', [SuggestionController::class, 'getUserSuggestions'])
        ->name('showSuggestionsForUser');
    Route::get('/suggestions/create', [SuggestionController::class, 'create'])
        ->name('suggestions.create');
    Route::post('/suggestions', [SuggestionController::class, 'store'])
        ->name('suggestions.store');
});

Route::middleware('guest')->group(function () {
    Route::get('/complaints', [ComplaintController::class, 'getVisibleComplaints'])
        ->name('showComplaintsForGuest');

    Route::get('/suggestions', [SuggestionController::class, 'getVisibleSuggestions'])
        ->name('showSuggestionsForGuest');
});

Route::get('/suggestions/{suggestion} ', [SuggestionController::class, 'show'])
    ->name('suggestions.show');

Route::get('/complaints/{complaint} ', [ComplaintController::class, 'show'])
    ->name('complaints.show');
