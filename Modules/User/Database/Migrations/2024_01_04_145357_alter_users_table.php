<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\User\Entities\User;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(User::TABLE_NAME, function (Blueprint $table) {
            $table->boolean(User::FIELD_IS_ADMIN)->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(User::TABLE_NAME, function (Blueprint $table) {
            $table->dropColumn(User::FIELD_IS_ADMIN);
        });
    }
}
