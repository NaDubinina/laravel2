<?php

namespace Modules\User\Database\Seeders;

use DB;
use Hash;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'Admin@admin.com',
            'password' => Hash::make('adminadmin'),
            'is_admin' => true,
        ]);

        DB::table('users')->insert([
            'name' => 'login',
            'email' => 'login@login.com',
            'password' => Hash::make('loginlogin'),
        ]);

        DB::table('users')->insert([
            'name' => 'test',
            'email' => 'test@test.com',
            'password' => Hash::make('testtest'),
        ]);
    }
}
