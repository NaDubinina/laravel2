<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Modules\ComplaintAndSuggestion\Entities\Complaint;
use Modules\ComplaintAndSuggestion\Entities\Suggestion;
use Carbon\Carbon;

/**
 * @property integer            $id
 * @property string             $name
 * @property string             $email
 * @property string             $password
 * @property boolean            $is_admin
 * @property Carbon|null        $created_at
 * @property Carbon|null        $updated_at
 *
 * @property-read  Complaint[]  $complaints
 * @property-read  Suggestion[] $suggestions
 * @property-read  bool         $isAdmin
 *
 * @extends Authenticatable
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    public const FIELD_ID = 'id';
    public const FIELD_NAME = 'name';
    public const FIELD_EMAIL = 'email';
    public const FIELD_PASSWORD = 'password';
    public const FIELD_IS_ADMIN = 'is_admin';
    public const TABLE_NAME = 'users';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_ID,
        self::FIELD_NAME,
        self::FIELD_EMAIL,
        self::FIELD_PASSWORD,
        self::FIELD_IS_ADMIN
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin()
    {
        return $this->is_admin === 1;
    }

    public function complaints(): HasMany
    {
        return $this->hasMany(Complaint::class);
    }

    public function suggestions(): HasMany
    {
        return $this->hasMany(Suggestion::class);
    }
}
